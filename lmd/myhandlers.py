from rapidsms.contrib.handlers import KeywordHandler, PatternHandler
from . smsvalidator import validate_nomor_poli,validate_kategori, validate_date,validate_date_reservation
from mozysms.sendsms import send_sms, get_antrian
#from mozysms.sendsms import insert_db
from datetime import datetime
import requests
import logging
import urllib
from xml.etree import ElementTree as ET

help_text = {
	'aaa' : 'help for aaa',
	'bbb' : 'help for bbb',
	'ccc' : 'help for ccc'
}

class HelpHandler(KeywordHandler):
	keyword = 'help|hlp|bantuan'
	def help(self):
		self.respond('perintah yang dibolehkan adalah AAA, BBB dan CCC. kirim HELP <command> untuk perintah spesifik.')

	def handle(self,text):
		text = text.strip().lower()
		if text == 'aaa':
			self.respond(help_text['aaa'])
		elif text == 'bbb':
			self.respond(help_text['bbb'])
		elif text == 'ccc':
			self.respond(help_text['ccc'])
		else:
			self.help()	

class DharmaisAntrianHandler(PatternHandler):
	pattern=r'^(\w+) ([^\s]+) (\d+) (\w+)$'

	def handle(self, reg , date, nomormr, kategori):
		logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
		logger = logging.getLogger(__name__)
		logger.info("================================================")
		logger.info("         MASUK DHARMAISANTRIANHANDLER           ")
		logger.info("================================================")
		message1 = 'Terima Kasih telah menggunakan layanan SMS Reservation RSK-Dharmais. Permintaan anda sedang diproses, mohon menunggu'
		# username = 'dharmais'
		# password = 'P4ssw0rd'
		# sendertext = 'RS-DHARMAIS'
		username = 'lmd'
		password = 'r4h4s14LMD'
		sendertext = 'Media Info'
		destination1 = self.msg.connection.identity
		destination = destination1[1:]
		# send_sms(username,password,sendertext,message1,destination)
		message=''
		
		if reg.lower() != 'reg':
			message = 'Format Salah. Gunakan format REG<spasi>tanggal<spasi>NomorMR<ID Poli>, contoh REG 12-05-2017 123456 C'
			send_sms(username,password,sendertext,message,destination)
		elif validate_date(date) is not True:
			message = 'Mohon maaf, format tanggal salah. Gunakan format dd-mm-yyyy'
			send_sms(username,password,sendertext,message,destination)
		elif validate_date_reservation(date) is not True:
			message = 'Pemesanan ticket antrian hanya bisa dilakukan H-2, H-1, dan hari H sebelum jam 5 pagi'
			send_sms(username,password,sendertext,message,destination)
		elif validate_kategori(kategori) is not True:
			message = 'Gunakan ID Poli C atau D'
			send_sms(username,password,sendertext,message,destination)
		else:
			# xml = get_antrian(nomormr,destination,date,kategori)
			# root = ET.fromstring(xml)
			# status = root[0].find('status').text
			# if status == 'false':
			# 	message = root[0][1].text
			# elif status == 'true':
			# 	antrian = root[0].find('NomorReservasi').text
			# 	antrian1 = antrian
			# 	nama = root[0].find('Nama').text
			# 	tgl_tombol = datetime.strptime(date,'%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
			# 	tgl_panggil = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
			# 	message = 'No MR: '+nomormr+', Nama: '+nama+', Antrian : '+antrian+', Kode Layanan : '+kategori+', Tanggal : '+date+', Jam pelayanan SEP : 07:00 - 08:00. Terimakasih'
			# 	send_sms(username,password,sendertext,message,destination)
			# 	insert_db(antrian,antrian1,kategori,"BELUM",tgl_tombol,tgl_panggil)

			message = 'No MR: 301294, Nama: TEGUH SAKTI PERMADI, TN, Antrian : 1, Kode Layanan : '+kategori+', Tanggal : '+date+', Jam pelayanan SEP : 07:00 - 08:00. Terimakasih'
			send_sms(username,password,sendertext,message,destination)

		self.respond(message)
		#send_sms(username,password,sendertext,message,destination)
		logger.info("================================================")
		logger.info("        KELUAR DHARMAISANTRIANHANDLER           ")
		logger.info("================================================")