from datetime import datetime
valid_poli = ['pol01', 'pol02','pol03']
valid_kategori = ['C','D']

def validate_nomor_poli(kodepoli):
	kode_poli = kodepoli.lower()
	for b in valid_poli:
		if b == kodepoli:
			return True

	return False

def validate_kategori(kategori):
	for n in valid_kategori:
		if n == kategori:
			return True
	return False

def validate_date(date): 
		try:
			print (datetime.strptime(date, '%d-%m-%Y'))
			return True
		except :
			return False

def validate_date_reservation(date):
	try:
		d0 = datetime.now().day
		d1 = datetime.strptime(date, '%d-%m-%Y').day
		d2 = datetime.now().month
		d3 = datetime.strptime(date, '%d-%m-%Y').month
		d4 = datetime.now().year
		d5 = datetime.strptime(date, '%d-%m-%Y').year

		monthvalid = d3 - d2
		datevalid = d1 - d0
		yearvalid = d5 - d4
		h0 = datetime.now()
		print("VALIDATE DATE")
		print(abs(datevalid))
		print(abs(monthvalid))
		print(abs(yearvalid))
		if abs(yearvalid) == 0:
			if abs(monthvalid) == 0:
		 		if 0<=abs(datevalid)<=2:
					if abs(datevalid)==0:
						if h0.hour < 5:
							return True
						else:
							return False
					else:
						return True
				else:
					return False
			elif abs(monthvalid) == 1:
				if d2 == 1 or d2 == 3 or d2 ==5 or d2 == 7 or d2 == 8 or d2 == 10 or d2 == 12:
					if abs(datevalid)==29 or abs(datevalid)==30:
						return True
					else:
						return False
				elif d2 == 2:
					if 26<= abs(datevalid) <= 28:
						return True
					else:
						return False
				elif d2 == 4 or d2 == 6 or d2 == 9 or d2 == 11:
					if abs(datevalid)==28 or abs(datevalid)==29:
						return True
					else:
						return False
				else:
					return False
			else:
				return False
		elif abs(yearvalid) == 1:
			if abs(monthvalid) == 11:
				if abs(datevalid) == 29 or abs(datevalid) == 30:
					return True
				else:
					return False
			else:
				return False
		else:
			return False
	except:
		return False
