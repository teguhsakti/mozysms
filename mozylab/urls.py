from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
#adding by asri djufri
from rapidsms.backends.kannel.views import KannelBackendView

# adding by Sakti
from mozysms.views import keyword, keyword_new, keyword_detail, keyword_edit, post_log_msg
#from django.conf.urls.defaults import *

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # RapidSMS core URLs
    url(r'^accountss/', include('rapidsms.urls.login_logout')),
    # url(r'^$', 'rapidsms.views.dashboard', name='rapidsms-dashboard'),  
    # RapidSMS contrib app URLs
    url(r'^httptester/', include('rapidsms.contrib.httptester.urls')),
    url(r'^messagelog/', include('rapidsms.contrib.messagelog.urls')),
    url(r'^messaging/', include('rapidsms.contrib.messaging.urls')),
    url(r'^registration/', include('rapidsms.contrib.registration.urls')),   
    # Third party URLs
    url(r'^selectable/', include('selectable.urls')),    

    # adding by asri djufri
    url(r"^backend/kannel-fake-smsc/$",KannelBackendView.as_view(backend_name="kannel-fake-smsc")),
    url(r"^backend/kannel-usb0-smsc/$",KannelBackendView.as_view(backend_name="kannel-usb0-smsc")),

    url(r'^accounts/login/$', 'mozysms.views.login_page', name="login_page"),
    url(r'^accounts/logout/$', 'mozysms.views.logout_page', name="logout_page"),
    url(r'^$', 'mozysms.views.home'),
    url(r'^home/$', 'mozysms.views.home', name="home"),
    url(r'^settingflood/$', 'mozysms.views.setting_flood', name="params"),
    url(r'^antrian/$', 'mozysms.views.antrian_log', name="antrian"),
    url(r'^blacklist/$', 'mozysms.views.blacklist', name="blacklist"),
    url(r'^blacklist/(?P<id>.*)/delete$', 'mozysms.views.delete_blacklist'), 
    url(r'^settingpoli/$', 'mozysms.views.setting_poli', name="setting_poli"),    
    url(r'^settinggeneral/$', 'mozysms.views.setting_general', name="setting_general"),    
    url(r'^settingpoli/(?P<id>.*)/delete$', 'mozysms.views.delete_poli'), 
    url(r'^log/antrian/$', 'mozysms.views.log_antrian', name="log_antrian"),
    url(r'^log/message/$', 'mozysms.views.log_msg', name="log_msg"),
    url(r'^log/post-message/$', 'mozysms.views.post_log_msg', name="post_log_msg"),

    url(r'^antrian/monitor/$', 'mozyqueue.views.monitor', name="monitor"),
    url(r'^antrian/tiket/$', 'mozyqueue.views.tiket', name="tiket"),
    url(r'^antrian/loket/$', 'mozyqueue.views.loket', name="loket"),

    # adding by Sakti
    url(r'^keyword/$', keyword, name="keyword"),
    url(r'^keyword/new/$', keyword_new, name='keyword_new'),
    url(r'^keyword/(?P<pk>\d+)/$', keyword_detail, name='keyword_detail'),
    url(r'^keyword/(?P<pk>\d+)/edit/$', keyword_edit, name='keyword_edit'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
