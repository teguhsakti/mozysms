CREATE VIEW V_MESSAGE_INPUT AS(
  SELECT messagelog_message.connection_id,
       messagelog_message.direction,
       messagelog_message.date,
       messagelog_message.text,
       rapidsms_connection.identity,
       rapidsms_connection.created_on,
       rapidsms_connection.modified_on
  FROM    mozysms.messagelog_message messagelog_message
       INNER JOIN
          mozysms.rapidsms_connection rapidsms_connection
       ON (messagelog_message.connection_id = rapidsms_connection.id)
 WHERE (messagelog_message.direction = 'I')
 )
