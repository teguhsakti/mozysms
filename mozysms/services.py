from . models import MRFake, Poli
import random

class DharmaisMRProvider():
	
	def validate_MR(self, mr_number):
		pass

class DharmaisAntrianAdaptor():
	def get_nomor_antrian(self, loket_target):
		pass
	def get_latest_antrian(self, loket_target):
		pass
	def get_current_antrian(self, loket_target):
		pass


class FakeMRProvider():

	def validate_MR(self, mr_number):
		dbmr = MRFake.objects.filter(no_mr=mr_number).first()
		result=None
		if dbmr:
			result = {'is_success':True, 'code':0, 'msg':'data valid','data':{'name':dbmr.nama, 'no_mr':dbmr.no_mr}}
		else:
			result = {'is_success':False, 'code':100, 'msg':'data tidak valid','data':None}
		return result

class FakeAntrianAdaptor():
	def get_nomor_antrian(self, loket_target):
		return random.randint(1, 500)

	def get_latest_antrian(self, loket_target):
		return random.randint(1, 500)
	def get_current_antrian(self, loket_target):
		return random.randint(1, 500)


