import requests
import urllib
#import MySQLdb
import logging
from datetime import datetime
from xml.etree import ElementTree as ET



def send_sms(username, password, sendertext, message, destination):
        #result = False
        logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
        logger = logging.getLogger(__name__)
        logger.info("================================================")
        logger.info("              WEBSERVIS SEND_SMS                ")
        logger.info("================================================")
        URL = "https://sms.lintasmediadanawa.com/SMSBulkService/SendMessage?"
        params = "username="+username+"&password="+password+"&sendertext="+sendertext+"&message="+message+"&destination="+destination
        web_service = URL + params
        response_url = requests.get(web_service)

        # if str(response_url.status_code) == '200':
        # 	root = ET.parse(urllib.urlopen(web_service)).getroot()
        # 	status = root[0].text
        # 	description = root[1].text   
        # 	if status == 0:
        #                 result = True
        #                 print(status)
        #                 print(description)
        # 	else:
        #                 result = False
        #                 print(status)
        #                 print(description)
        # else:
        # 	result = False
        logger.info("================================================")
        logger.info("              DONE SEND_SMS                ")
        logger.info("================================================")
        return True
        # return True



def get_antrian(nomormr,destination,tanggal,kategori):
        logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
        logger = logging.getLogger(__name__)
        logger.info("================================================")
        logger.info("             WEBSERVIS GET_ANTRIAN              ")
        logger.info("================================================")
        #IP Private
        URL = "http://192.168.7.162/webservice/kemkes/pendaftaran-online/buatAntrianSMS"
        #IP Public
        #URL = "http://182.23.88.116/webservice/kemkes/pendaftaran-online/buatAntrianSMS"
        dest = "0"+destination[2:]
        print ("NO. DESTINATION : "+dest)
        xml = "<?xml version=\"1.0\"?>"+"<reservasi>"+"<data>"+"<NoCM>"+nomormr+"</NoCM>"+"<noKontak>"+dest+"</noKontak>"+"<TglKunjungan>"+tanggal+"</TglKunjungan>"+"<idpoli>"+kategori+"</idpoli>"+"</data>"+"</reservasi>"
        headers = {'Content-Type': 'application/xml'}
        response_url = requests.post(URL, data=xml, headers=headers)
        

        if str(response_url.status_code)  == '200':
                root = ET.fromstring(response_url.text)
                status = root[0].find('status').text
                if status == 'false':
                        pesan = root[0][1].text
                        logger.info(pesan)
                elif status == 'true':
                        antrian = root[0].find('NomorReservasi').text
                        nama = root[0].find('Nama').text
                        logger.info(antrian)
                        logger.info(nama)

        logger.info("================================================")
        logger.info("               DONE GET_ANTRIAN                 ")
        logger.info("================================================")
        return response_url.text


# def insert_db(antrian,antrian1,layanan,ket,tgltombol,tglpanggil):
#         logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
#         logger = logging.getLogger(__name__)
#         conn= MySQLdb.connect(host="192.168.32.202",user="root",passwd="",db="data_antrian")
#         x = conn.cursor()
#         try:
#                 logger.info("================================================")
#                 logger.info("               MASUK INSERT_DB                  ")
#                 logger.info("================================================")
#                 #x.execute("""SELECT * FROM antrian_rj_upo limit 1""")
#                 if layanan == 'C':
#                         x.execute("""INSERT INTO antrian_rj_upo_2 (no_antrian,no_reg,layanan,ket,tgl_tombol,tgl_panggil) VALUES (%s,%s,%s,%s,%s,%s)""",(antrian,antrian1,layanan,ket,tgltombol,tglpanggil))
#                 elif layanan == 'D':
#                         x.execute("""INSERT INTO antrian_rj_upo_1 (no_antrian,no_reg,layanan,ket,tgl_tombol,tgl_panggil) VALUES (%s,%s,%s,%s,%s,%s)""",(antrian,antrian1,layanan,ket,tgltombol,tglpanggil))
#                 logger.info("BERHASIL INSERT DATABASE")
#                 conn.commit()
#         except Exception,e:
#                 conn.rollback()
#                 logger.info(str(e))

#         conn.close()
#         logger.info("================================================")
#         logger.info("              KELUAR INSERT_DB                  ")
#         logger.info("================================================")

# tgl_tombol = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
# tgl_panggil = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


# get_antrian('12922','2017-08-13','Q')
# send_sms('lmd','r4h4s14LMD','mobdev','sms python','081230282889')

# URL = "https://sms.lintasmediadanawa.com/SMSBulkService/SendMessage?"
# params = "sendertext=mobdev&username=lm&password=r4h4s14LMD&message=sms webservis python&destination=081230282889"
# web_service = URL + params

# response_url = requests.get(web_service)
# print(requests.get(web_service).status_code)
# result = True

# if str(response_url.status_code) == '200':
# 	root = ET.parse(urllib.urlopen(web_service)).getroot()
# 	status = root[0].text
# 	description = root[1].text
# 	if status == 0:
# 		result = True
# 	else:
# 		result = False
# 		print(status)
# 		print(description)
# else:
# 	result = False
      
# print(result)
