from django import template
from mozysms.models import AppSetting
import datetime

register = template.Library()

@register.filter(name="get_appsetting")
def get_appsetting(key):	
	param = AppSetting.objects.filter(key=key).first()
	if param:
		return param.value
	return None
@register.filter(name='get_jam')
def get_jam(key):
	try:
		t = datetime.datetime.strptime(key,'%H:%M').time()
		return t.hour
	except:
		return 0
	
	
@register.filter(name='get_menit')
def get_menit(key):	
	try:
		t = datetime.datetime.strptime(key,'%H:%M').time()
		return t.minute
	except:
		return 0

@register.filter(name="attr_ischeckboxchecked")
def attr_ischeckboxchecked(key):
	param = AppSetting.objects.filter(key=key).first()
	if param:
		if param.value =='TRUE':
			return 'checked'
	return ''
	


