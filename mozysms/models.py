from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True,related_name="user_profile")
	number_receiver = models.CharField(max_length=25,null=True)
	create_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)
	create_by = models.ForeignKey(User,related_name ='user_profile2',null=True)
	update_by = models.ForeignKey(User,related_name ='user_profile3',null=True)
	def __str__(self):
		return self.user.username

	def create_user_profile(sender, instance, created, **kwargs):  
		if created:  
		   profile, created = UserProfile.objects.get_or_create(user=instance)		
	post_save.connect(create_user_profile, sender=User)

class Keyword(models.Model):
	message_request = models.CharField(max_length=300, unique=True)
	message_response_true = models.CharField(max_length=300)
	message_response_false = models.CharField(max_length=300)	
	number_receiver = models.CharField(max_length=25)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	create_by = models.ForeignKey(User,related_name ='keyword1',null=True)
	update_by = models.ForeignKey(User,related_name ='keyword2',null=True)
	def __str__(self):
		return self.message_request

class DaftarNoAntrian(models.Model):
	no_mr = models.CharField(max_length=50)
	nama_lengkap = models.CharField(max_length=50)
	target_poly = models.CharField(max_length=25)
	no_antrian = models.IntegerField()
	kode_booking = models.IntegerField(null=True)
	visit_date = models.DateTimeField(verbose_name='visit date')
	created_at = models.DateTimeField(verbose_name="created at", null=True, auto_now=True)

class Poli(models.Model):
	kode_poli =models.CharField(max_length=25,primary_key=True);
	nama = models.CharField(max_length=25);
	no_antrian_max =models.IntegerField(default=0)
	no_antrian_min = models.IntegerField(default=0)	
	jumlah_nomor = models.IntegerField(default=0)
	max_day_before_visit = models.IntegerField(default=0)
	is_enable =models.BooleanField(default=True)
	updated_at = models.DateTimeField(verbose_name="created at", null=True, auto_now=True)
	updated_by = models.CharField(max_length=25)
	def __str__(self):
		return self.kode_poli+' ('+self.nama+')'

class AppSetting(models.Model):
	key = models.CharField(max_length=150)
	value = models.CharField(max_length=150)

class AppCounter(models.Model):
	key = models.CharField(max_length=50)
	value = models.IntegerField()

class BlacklistNumber(models.Model):
	no_telp = models.CharField(max_length=15)
	reason = models.CharField(max_length=100)
	is_enable = models.BooleanField(default=True)
	created_at =models.DateTimeField(verbose_name="created at", auto_now=True)




class MRFake(models.Model):
	no_mr = models.CharField(primary_key=True, max_length=25)
	nama=models.CharField(max_length=100)
	# alamat= models.CharField(max_length=100);
	def __str__(self):
		return self.no_mr

