# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mozysms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_request', models.TextField(unique=True, max_length=50)),
                ('message_response_true', models.TextField(max_length=50)),
                ('message_response_false', models.TextField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('create_by', models.ForeignKey(related_name='keyword1', to=settings.AUTH_USER_MODEL, null=True)),
                ('update_by', models.ForeignKey(related_name='keyword2', to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
