# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mozysms', '0003_auto_20170602_1133'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(related_name='user_profile', primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('gsm_number', models.CharField(max_length=25, null=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('create_by', models.ForeignKey(related_name='user_profile2', to=settings.AUTH_USER_MODEL, null=True)),
                ('update_by', models.ForeignKey(related_name='user_profile3', to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='keyword',
            name='message_request',
            field=models.CharField(unique=True, max_length=300),
        ),
        migrations.AlterField(
            model_name='keyword',
            name='message_response_false',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='keyword',
            name='message_response_true',
            field=models.CharField(max_length=300),
        ),
    ]
