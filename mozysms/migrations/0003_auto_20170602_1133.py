# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mozysms', '0002_keyword'),
    ]

    operations = [
        migrations.AlterField(
            model_name='keyword',
            name='message_request',
            field=models.TextField(unique=True, max_length=300),
        ),
        migrations.AlterField(
            model_name='keyword',
            name='message_response_false',
            field=models.TextField(max_length=300),
        ),
        migrations.AlterField(
            model_name='keyword',
            name='message_response_true',
            field=models.TextField(max_length=300),
        ),
    ]
