# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mozysms', '0004_auto_20170605_0954'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='gsm_number',
            new_name='number_receiver',
        ),
        migrations.AddField(
            model_name='keyword',
            name='number_receiver',
            field=models.CharField(default=1, max_length=25),
            preserve_default=False,
        ),
    ]
