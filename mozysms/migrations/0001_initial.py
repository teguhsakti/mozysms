# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AppCounter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=50)),
                ('value', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='AppSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=150)),
                ('value', models.CharField(max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name='BlacklistNumber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_telp', models.CharField(max_length=15)),
                ('reason', models.CharField(max_length=100)),
                ('is_enable', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name=b'created at')),
            ],
        ),
        migrations.CreateModel(
            name='DaftarNoAntrian',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_mr', models.CharField(max_length=50)),
                ('nama_lengkap', models.CharField(max_length=50)),
                ('target_poly', models.CharField(max_length=25)),
                ('no_antrian', models.IntegerField()),
                ('kode_booking', models.IntegerField(null=True)),
                ('visit_date', models.DateTimeField(verbose_name=b'visit date')),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name=b'created at', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MRFake',
            fields=[
                ('no_mr', models.CharField(max_length=25, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Poli',
            fields=[
                ('kode_poli', models.CharField(max_length=25, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=25)),
                ('no_antrian_max', models.IntegerField(default=0)),
                ('no_antrian_min', models.IntegerField(default=0)),
                ('jumlah_nomor', models.IntegerField(default=0)),
                ('max_day_before_visit', models.IntegerField(default=0)),
                ('is_enable', models.BooleanField(default=True)),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'created at', null=True)),
                ('updated_by', models.CharField(max_length=25)),
            ],
        ),
    ]
