from django.forms import  ModelForm
from django import forms 
from django.contrib.auth.forms import UserCreationForm,UserChangeForm

from .models import Poli, BlacklistNumber, Keyword

class KeywordForm(ModelForm):
	class Meta:
		model = Keyword
		fields =['message_request','message_response_true', 'message_response_false','number_receiver']
		
	def __init__(self, *args, **kwargs):
	    super(KeywordForm, self).__init__(*args, **kwargs)
	    for field in iter(self.fields):
		        self.fields[field].widget.attrs.update({
			            'class': 'form-control'
		    		})	

class FormPoli(ModelForm):
	class Meta:
		model = Poli
		fields =['kode_poli','nama', 'no_antrian_max','no_antrian_min','jumlah_nomor','max_day_before_visit']
	

class FormBlacklistNumber(ModelForm):
	class Meta:	
		model = BlacklistNumber
		fields =['no_telp', 'reason']

class Form_login(forms.Form):
	username= forms.CharField(label='Username', max_length=25, widget=forms.TextInput(attrs={'id':'username'}))
	password= forms.CharField(label='Password', max_length=25, widget=forms.PasswordInput(attrs={'id':'password'}))	