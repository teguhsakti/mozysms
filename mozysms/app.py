from rapidsms.apps.base import AppBase
from . models import BlacklistNumber, AppSetting, AppCounter, Keyword
from .dbviews import MessageFromOutSide
from datetime import date, datetime, time
from mozysms.sendsms import send_sms
import logging


class AntiFlooding(AppBase):
	def count_flooding(self, identity):
		dbcounter = AppCounter.objects.filter(key=identity).first()
		if dbcounter:
			dbcounter.value = dbcounter.value+1
		else:
			dbcounter = AppCounter(key=identity, value=1)
		dbcounter.save()

	def do_anti_flooding(self, identity):		
		dbantifloodenable = AppSetting.objects.filter(key='ANTI_FLOODING_ENABLE').first()
		if dbantifloodenable:
			if dbantifloodenable.value == 'TRUE':			
				dbcount = AppCounter.objects.filter(key=identity).first()
				count = 0
				if dbcount:
					count = dbcount.value

				dbthreshold = AppSetting.objects.filter(key='ANTI_FLOODING_THRESHOLD').first()
				reason_txt = AppSetting.objects.filter(key='RESPONSE_ANTI_FLOODING_TEXT').first().value
				threshold = 0
				# try:
				threshold = int(dbthreshold.value)
				if count >= threshold:
					dbbl = BlacklistNumber.objects.filter(no_telp=identity).first()
					if not dbbl:
						dbbl = BlacklistNumber(no_telp=identity,reason=reason_txt, is_enable=True)
						dbbl.save()
						#reset db count
						dbcount.value=0
						dbcount.save()
				# except:
				# 	pass



	def handle(self, msg):
		logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
		logger = logging.getLogger(__name__)
		logger.info("================================================")
		logger.info("           MASUK APP BASE MOZY SMS              ")
		logger.info("================================================")
		identity = msg.connection.identity
		self.count_flooding(identity)
		self.do_anti_flooding(identity)

		message1 = 'Terima Kasih telah menggunakan layanan MozySMS. Permintaan anda sedang diproses, mohon menunggu'
		username = 'lmd'
		password = 'r4h4s14LMD'
		sendertext = 'Media Info'
		destination = identity[1:]
		message = ''
		print(destination)
		
		receiver = Keyword.objects.filter(message_request=msg)
		if len(receiver) > 0 :
			send_sms(username,password,sendertext,message1,destination)
			receiver1 = receiver.first()
			msg_request = str(receiver1.message_request)
			msg_response_true = str(receiver1.message_response_true)
			msg_response_false = str(receiver1.message_response_false)
			print(msg_request)
			print(msg_response_true)
			print(msg_response_false)
			if msg.text.lower() == msg_request.lower():
				print("if1")
				message = msg_response_true
				msg.respond(msg_response_true)
				send_sms(username,password,sendertext,message,destination)
				return True
			else:
				print("else1")
				message = msg_response_false
				msg.respond(msg_response_false)
				send_sms(username,password,sendertext,message,destination)
				return True
			return False
		else:
			return False

		logger.info("================================================")
		logger.info("          KELUAR APP BASE MOZY SMS              ")
		logger.info("================================================")

	def filter(self, msg):
		identity = msg.connection.identity
		print "identity is "+identity
		bl = BlacklistNumber.objects.filter(no_telp=identity).first()
		if bl:
			blresponmode = AppSetting.objects.filter(key='RESPONSE_BLACKLIST_MODE').first()
			resp_txt = AppSetting.objects.filter(key='BLACKLIST_DEFAULT_RESP_TEXT').first()
			if blresponmode.value == '0':
				logger = logging.getLogger(__name__)
				logger.debug('phone number with identity '+identity+' is block and respond blocklist is silent')				

			elif blresponmode.value =='1':				
				msg.respond(resp_txt.value)

			elif blresponmode.value == '2':
				msg.respond(bl.reason)
			elif blresponmode.value =='3':
				msg.respond(resp_txt.value+' REASON:'+bl.reason)
			else:
				msg.respond(resp_txt.value+' REASON:'+bl.reason)

			return True			# block message		
			
		return False
		
	def default(self, msg):
		logging.basicConfig(filename='mozylab/core.log',level=logging.INFO, format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %I:%M:%S %p')
		logger = logging.getLogger(__name__)
		logger.info("================================================")
		logger.info("           MASUK DEFAULT APP BASE               ")
		logger.info("================================================")
		username = 'dharmais'
		password = 'P4ssw0rd'
		sendertext = 'RS-DHARMAIS'
		destination1 = msg.connection.identity
		destination = destination1[1:]
		angkaPertama = destination[0:1]
		if angkaPertama == "0" or angkaPertama == "6":
			message='Format Salah. Gunakan format REG<spasi>tanggal<spasi>NomorMR<ID Poli>, contoh REG 12-05-2017 123456 C'
			send_sms(username,password,sendertext,message,destination)
		else:
			logger.info("Format No Hp, Angka pertama bukan 0 atau 6")
		logger.info("================================================")
		logger.info("          KELUAR DEFAULT APP BASE               ")
		logger.info("================================================")