from rapidsms.contrib.handlers import KeywordHandler, PatternHandler
from .models import Poli, DaftarNoAntrian, AppSetting
from .services import FakeMRProvider, FakeAntrianAdaptor 
from  django.db.models import Q
from datetime import date, datetime, time
import random

class DharmaisAntrianHandler(PatternHandler):
	def validate_nomor_poli(self, kodepoli):
		dbpoli = Poli.objects.filter(kode_poli=kodepoli).first()
		if dbpoli:
			if dbpoli.kode_poli == kodepoli:
				return True
		return False
	
	def validate_nomor_mr(self, nomormr):
		mrprovider = FakeMRProvider()
		self.mrdata = mrprovider.validate_MR(nomormr)
		if self.mrdata['is_success']:
			return True
		return False
	def validate_tanggalreservasi(self, tanggalreservasi,kodepoli):
		dbpoli = Poli.objects.filter(kode_poli=kodepoli).first()
		print 'tanggalreservasi : '+str(tanggalreservasi.day)
		max_day_before_visit =dbpoli.max_day_before_visit		
		current_date = date.today()
		deltadate = tanggalreservasi - current_date
		deltaday = deltadate.days
		if deltaday > max_day_before_visit:
			return False
		return True	
		
	def get_nomor_antrian(self, kodepoli):
		a = FakeAntrianAdaptor()
		return a.get_nomor_antrian(kodepoli)

	pattern=r'^reg (\w+) (\w+) (\d{2})(\d{2})(\d{4})$'

	def handle(self, kodepoli, nomormr, tanggal, bulan, tahun):
		print self.router
		bulan=int(bulan)
		tanggal=int(tanggal)
		tahun = int(tahun)
		
		print 'tanggal : '+str(tanggal)
		print 'bulan : '+str(bulan)
		print 'tahun : '+str(tahun)
		
		target_visit_date=date(tahun,bulan,tanggal)

		#validasi tanggal
		if tanggal > 31:
			self.respond('maximum tanggal adalah 31')
			return
		if bulan > 12:
			self.respond('maximum bulan adalah 12')
			return
		if tahun > 2999:
			self.respond('maximum tahun adalah 2999')
			return

		target_visit_date=date(tahun,bulan,tanggal)

		#validasi kode poli
		dbpoli = Poli.objects.filter(kode_poli=kodepoli).first()
		if dbpoli is None:
			self.respond('nomor poli tidak dikenali')
			return
		#validasi maximum before visit
		max_day_before_visit =dbpoli.max_day_before_visit		
		current_date = date.today()
		
		deltadate = target_visit_date - current_date
		deltaday = deltadate.days
		if deltaday > max_day_before_visit:
			self.respond('tanggal reservasi maximum '+str(max_day_before_visit)+' hari sebelum tanggal visit')	
			return

		# if self.validate_nomor_poli(kodepoli) is not True:
		# 	self.respond('nomor poli tidak dikenali')
		# elif self.validate_tanggalreservasi(target_visit_date,kodepoli) is not True:
		# 	self.respond('tanggal reservasi maximum '++' hari sebelum tanggal visit')	
			
		if self.validate_nomor_mr(nomormr) is not True:
			self.respond('nomor medical record anda tidak dapat ditemukan atau tidak valid')
			
		else:
			# now = datetime.now()
			pub_date = date.today()
			min_pub_date_time = datetime.combine(pub_date, time.min) 
			max_pub_date_time = datetime.combine(pub_date, time.max)
			start_date_range=(min_pub_date_time,max_pub_date_time)
			dbantrian = DaftarNoAntrian.objects.filter(Q(no_mr = self.mrdata['data']['no_mr']) & Q(created_at__range=start_date_range)).first()
			if dbantrian:
				no_antrian=dbantrian.no_antrian
				self.respond('Nomor MR anda adalah '+dbantrian.no_mr+' atas nama'+dbantrian.nama_lengkap+' nomor antrian anda adalah : '+str(no_antrian))
			else:
				no_antrian = self.get_nomor_antrian(kodepoli)
				kodebooking= random.randint(1, 9999)
				dbantrian = DaftarNoAntrian(no_mr=self.mrdata['data']['no_mr'], nama_lengkap=self.mrdata['data']['name'],target_poly=kodepoli,no_antrian=no_antrian,kode_booking=kodebooking, visit_date = target_visit_date)
				dbantrian.save()
				self.respond('Nomor MR anda adalah '+dbantrian.no_mr+' atas nama'+dbantrian.nama_lengkap+' nomor antrian anda adalah : '+str(no_antrian))
