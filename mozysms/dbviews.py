from django.db import  models

class MessageFromOutSide(models.Model):
	connection_id = models.IntegerField(primary_key=True)
	direction =models.CharField(max_length=1)
	date = models.DateTimeField()
	identity= models.CharField(max_length=100)
	created_on = models.DateTimeField()
	modified_on = models.DateTimeField()
	class Meta:
		db_table="V_MESSAGE_INPUT"
