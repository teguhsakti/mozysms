from django.shortcuts import render,get_object_or_404,render_to_response
from django.http import HttpResponse, HttpResponseRedirect  
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from rapidsms.contrib.messagelog.models import Message

from .forms import FormPoli, FormBlacklistNumber, Form_login, KeywordForm
from .models import Poli, AppSetting, BlacklistNumber, DaftarNoAntrian, Keyword

from datetime import date
from datetime import datetime

def save_or_update_appsetting(key, value):
	target = AppSetting.objects.filter(key=key).first()
	if target:
		target.value=value
	else:
		target = AppSetting(key=key, value=value)
	target.save()
	return target

	#convert nilai integer string menjadi string time format hh:mm
def convert_number_to_time(strvalue):
	if int(strvalue) <10:
		strvalue= strvalue+strvalue
	return strvalue
# Create your views here.
@login_required
def log_msg(request):
	messages = Message.objects.all().order_by('-date')
	return render(request,'v1/log-message.html',{'user':request.user,'messages':messages})

@login_required
def post_log_msg(request):
	status = False
	nomor = request.POST.get("nomor")
	start = request.POST.get("start_date")
	end = request.POST.get("end_date")
	startstring = start + " 00:00"
	endstring = end + " 23:59"
	messages = Message.objects.filter(connection__identity__contains=nomor, direction="I", date__range=(startstring, endstring)).order_by('-date')
	count = Message.objects.filter(connection__identity__contains=nomor, direction="I", date__range=(startstring, endstring)).count()
	return render(request,'v1/post-message.html',{'user':request.user,'messages':messages,'count':count})

@login_required
def log_antrian(request):
	antrians = DaftarNoAntrian.objects.all()
	return render(request,'v1/log-antrian.html',{'user':request.user,'antrians':antrians})

@login_required
def setting_flood(request):

	dbenable = AppSetting.objects.filter(key='ANTI_FLOODING_ENABLE').first()
	dbenablerespond =AppSetting.objects.filter(key='RESPONSE_ANTI_FLOODING_ENABLE').first()
	dbresptext = AppSetting.objects.filter(key='RESPONSE_ANTI_FLOODING_TEXT').first()
	dbthreshold = AppSetting.objects.filter(key='ANTI_FLOODING_THRESHOLD').first()

	if request.POST:
		enableantiflood = request.POST.get('ANTI_FLOODING_ENABLE','FALSE')
		enablerespond = request.POST.get('RESPONSE_ANTI_FLOODING_ENABLE','FALSE')
		resptext = request.POST.get('RESPONSE_ANTI_FLOODING_TEXT', 'FALSE')
		threshold = request.POST.get('ANTI_FLOODING_THRESHOLD',1000)		
		if enablerespond =='on':
			enablerespond='TRUE'
		if enableantiflood =='on':
			enableantiflood = 'TRUE'
		if dbenable:
			dbenable.value = enableantiflood
		else:
			dbenable = AppSetting(key='ANTI_FLOODING_ENABLE', value=enableantiflood)
		
		if dbenablerespond:
			dbenablerespond.value = enablerespond
		else:
			dbenablerespond = AppSetting(key='RESPONSE_ANTI_FLOODING_ENABLE', value=enablerespond)

		if dbresptext:
			dbresptext.value = resptext
		else:
			dbresptext =AppSetting(key='RESPONSE_ANTI_FLOODING_TEXT', value=resptext)

		if dbthreshold:
			dbthreshold.value=threshold
		else:
			dbthreshold = AppSetting(key='ANTI_FLOODING_THRESHOLD', value=threshold)

		dbenable.save()
		dbenablerespond.save()
		dbresptext.save()
		dbthreshold.save()
	return render(request,'v1/setting-flood.html',{'user':request.user})

@login_required
def home(request):
	return render(request,'v1/index.html')

@login_required
def setting_poli(request):
	fpoli = FormPoli(request.POST)
	if request.POST :
		if fpoli.is_valid():
			kode_poli=fpoli.cleaned_data['kode_poli']
			nama = fpoli.cleaned_data['nama']
			no_antrian_max = fpoli.cleaned_data['no_antrian_max']
			no_antrian_min = fpoli.cleaned_data['no_antrian_min']
			jumlah_nomor = fpoli.cleaned_data['jumlah_nomor']
			max_day_before_visit = fpoli.cleaned_data['max_day_before_visit']
			poli = Poli(kode_poli=kode_poli, nama=nama,no_antrian_min=no_antrian_min, 
				no_antrian_max=no_antrian_max, jumlah_nomor=jumlah_nomor, max_day_before_visit=max_day_before_visit)
			poli.save()
	polis = Poli.objects.all()
	return render(request, 'v1/poli.html',{'polis':polis,'form':fpoli})

@login_required
def delete_poli(request,id):
	poli = Poli.objects.get(kode_poli=id)
	poli.delete()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def antrian_log(request):
	return render(request, 'v1/login.html')

@login_required
def blacklist(request):
	fblacklist = FormBlacklistNumber(request.POST)
	if request.POST:
		if fblacklist.is_valid():
			no_telp = fblacklist.cleaned_data['no_telp']
			reason = fblacklist.cleaned_data['reason']
			blacklist = BlacklistNumber(no_telp=no_telp, reason=reason)
			blacklist.save()

	blacklists= BlacklistNumber.objects.all()

	return render(request, 'v1/blacklist.html', {'blacklists':blacklists})

@login_required
def delete_blacklist(request, id):
	no2block = BlacklistNumber.objects.get(id=id)
	no2block.delete()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def setting_general(request):
	if request.POST:
		if request.POST.get('msg_setting_form'):
			sms_help_message = request.POST.get('SMS_HELP_MESSAGE','')
			sms_response_antrian = request.POST.get('SMS_RESP_NO_ANTRIAN','')	
			save_or_update_appsetting('SMS_RESP_NO_ANTRIAN',sms_response_antrian)
			save_or_update_appsetting('SMS_HELP_MESSAGE',sms_help_message)
		if request.POST.get('bl_setting_form'):
			resp_flood_mode = request.POST.get('RESPONSE_BLACKLIST_MODE','0')
			bl_default_resp_text = request.POST.get('BLACKLIST_DEFAULT_RESP_TEXT','')
			save_or_update_appsetting('RESPONSE_BLACKLIST_MODE',resp_flood_mode)
			save_or_update_appsetting('BLACKLIST_DEFAULT_RESP_TEXT',bl_default_resp_text)
		if request.POST.get('app_time_form'):
			smstime2starthour = request.POST.get('start_hour','0')
			smstime2startminute = request.POST.get('start_minute','0')
			smstime2stophour = request.POST.get('finish_hour','0')
			smstime2stophminute = request.POST.get('finish_minute')
			max_day_before_visit = request.POST.get('sms_max_day_before_visit')

			# agar menjadi format hh:mm
			time2start =smstime2starthour+':'+smstime2startminute
			time2stop = smstime2stophour+':'+smstime2stophminute
			save_or_update_appsetting('SMS_TIME2START',time2start)
			save_or_update_appsetting('SMS_TIME2STOP', time2stop)
			save_or_update_appsetting('SMS_MAX_DAY_BEFORE_VISIT', max_day_before_visit)

	return render(request, 'v1/setting-general.html')

def login_page(request):
	form = Form_login(request.POST)
	if request.POST:		
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request,user)
					return HttpResponseRedirect(request.GET.get('next','/home'))
				else:
					return HttpResponse("user belum aktif")
			else:
				return HttpResponse("tidak dapat di autentikasi")			
		else:
			print form.non_field_errors
			return HttpResponse("data tidak valid")
	return render(request, 'v1/login.html')

def logout_page(request):
	logout(request)
	return HttpResponseRedirect('/')

# Start Sakti

#view_keyword
@login_required
def keyword(request):
	keyword = Keyword.objects.all()
	objects = {
		'keyword':keyword,
	}
	return render(request,'v1/keyword.html',objects)

@login_required
def keyword_new(request):
	if request.method == 'POST':
		print("AKasdasdasdsaSES")
		form = KeywordForm(request.POST,request.FILES)
		print(form.is_valid)
		if form.is_valid():
			post = form.save(commit=False)
			post.create_by = request.user
			post.save()
			return HttpResponseRedirect('/keyword/')
	else:
		print("FORM")
		form = KeywordForm()
	return render(request,'v1/form_keyword.html',{'form':form})

@login_required
def keyword_detail(request):
	post = get_object_or_404(Keyword, pk=pk)
	form = KeywordForm(instance=post)
	return render(request,'v1/form_keyword.html',{'form':form})

@login_required
def keyword_edit(request,pk):
	post=get_object_or_404(Keyword, pk=pk)
	if request.method == 'POST':
		form = KeywordForm(request.POST, request.FILES, instance=post)
		if form.is_valid():
			post = form.save(commit=False)
			post.save()
			return HttpResponseRedirect('/keyword/')
	else:
		form = KeywordForm(instance=post)
	return render(request,'v1/form_keyword.html',{'form':form})