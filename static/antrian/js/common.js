function ExecRemote(url, successCallback, errorCallback, mydata){
    $.ajax({
        type:'POST',
        url:url,
        datatype : "json",
        contentType: "application/json; charset=utf-8",            
        data : JSON.stringify(mydata),
        success:function(data, textStatus,jQxhr ){
            successCallback(data);
            //alert('ini sukses '+data);
            },
        error:function(jqXhr,textStatus,errorThrown){
            errorCallback(textStatus);
            //alert('ini error '+textStatus);
        }

    });
};