// var element_no_antrian_id = 'noantrian';
// var element_loket_id ='noloket';
var delay_pooling = 1000;
var delay_button_enable =1000;
var monitor_url ='/antrian/monitor';
var loket_url = '/antrian/loket/';
var tiket_generator_url='/antrian/tiket/';


function ExecRemote(url, successCallback, errorCallback, mydata){
    $.ajax({
        type:'POST',
        url:url,
        datatype : "json",
        contentType: "application/json; charset=utf-8",            
        data : JSON.stringify(mydata),
        success:function(data, textStatus,jQxhr ){
            successCallback(data);
            //alert('ini sukses '+data);
            },
        error:function(jqXhr,textStatus,errorThrown){
            errorCallback(textStatus);
            //alert('ini error '+textStatus);
        }

    });
};

var Loket={
    runLoket:function(kode_poli){
        var data={kode_poli:kode_poli,action:'list'}

        setInterval(function(){
            ExecRemote(loket_url,Loket.updateDisplay,Loket.handleError, data)
        },2000);
        
        var data2 ={action:'get_current_antrian', kode_poli:kode_poli}
        setInterval(function(){
            ExecRemote(loket_url,Loket.updateDisplayCurrentAntrian,Loket.handleError, data2)
        },2000);

        //tambahan hilman
        var data3 ={action:'get_antrian_terakhir', kode_poli:kode_poli}
        setInterval(function(){
            ExecRemote(loket_url, Loket.updateDisplayCurrentAntrianTerakhir, Loket.handleError, data3)
        },5000);
        //end of hilman
        Loket.attachEvent2ElementbyClass('currentantri');
    },
    attachEvent2ElementbyClass:function(class_name){
        $('.'+class_name).click(function(){
            var action = $(this).attr('action');
            var antrian_id =$(this).attr('antrian_id');
            
            if(action=='panggil'){
                var data={antrian_id:antrian_id,action:'panggil'};
                ExecRemote(loket_url,Loket.updateDisplay,Loket.onExecuteActions, data);
            }else if(action == 'tunda'){
                var data={antrian_id:antrian_id,action:'tunda'};
                ExecRemote(loket_url,Loket.updateDisplay,Loket.onExecuteActions, data);
            }else if(action== 'done'){
                var data={antrian_id:antrian_id,action:'done'};
                ExecRemote(loket_url,Loket.updateDisplay,Loket.onExecuteActions, data);
            }else if(action=='reset'){
                var data={antrian_id:antrian_id, action:'reset'};
                ExecRemote(loket_url,Loket.updateDisplay,Loket.onExecuteActions, data);
            }
        })

    },
    onExecuteActions:function(data){

    },
    updateDisplayCurrentAntrian:function(data){
        if(data.is_ok){
            console.log(data.data);
            if (data.data){
                var no_antrian = data.data.no_antrian;
                $('#current_no_antrian').text(no_antrian);
                $('#btn_current_panggil').attr('antrian_id',data.data.id).removeAttr('disabled');
                $('#btn_current_tunda').attr('antrian_id',data.data.id).removeAttr('disabled');
                $('#btn_current_done').attr('antrian_id',data.data.id).removeAttr('disabled');
                $('#server_time').text(data.data.server_time);
            }else{
                $('#current_no_antrian').text('-');
                $('#btn_current_panggil').attr('disabled','true');
                $('#btn_current_tunda').attr('disabled','true');
                $('#btn_current_done').attr('disabled','true'); 
            }
            
        }

    },
    //hilman
    updateDisplayCurrentAntrianTerakhir:function(data){
        if(data.is_ok){
            var row_tmpl='';
            if(data.data){
                data.data.forEach(function(item){  
                    row_tmpl+="<tr>";
                    row_tmpl+="<td><b>"+item.no_antrian+'</b></td> ';
                    row_tmpl+="<td>"+item.status +'</td>';
                    row_tmpl+="<td><button class='btn antri_terakhir' action='reset' antrian_id='"+item.id+"' >"+ "Reset</button> ";
                    row_tmpl+="<button class='btn antri_terakhir btn-danger' action='done' antrian_id='"+item.id+"' >"+ "Selesai</button></td>";

                    row_tmpl+="</tr>";
                });
                $('#tbodyterakhir').empty();
                $('#tbodyterakhir').append(row_tmpl);
                Loket.attachEvent2ElementbyClass('antri_terakhir');
            }        
        }
    },
    updateDisplay:function(data){
        if(data.is_ok){
            var row_tmpl='';
            console.log(data);
            if(data.data){
                data.data.forEach(function(item){
                    row_tmpl+="<tr>";
                    row_tmpl+="<td class='text-danger'><h1>"+item.no_antrian+'</h1></td> ';
                    row_tmpl+="<td class='text-danger' >"+item.status +'</td>';       
                    row_tmpl+="<td><button class='btn antri btn-primary btn-lg' action='panggil' antrian_id='"+item.id+"' >"+ "Panggil</button>";
                    //row_tmpl+="<button class='btn btn-danger' action='tunda' antrian_id='"+item.id+"' >"+ "tunda</button>";
                    //row_tmpl+="<button class='btn btn-warning' action='done' antrian_id='"+item.id+"' >"+ "selesai</button></td>"

                    row_tmpl+="</tr>";                
                });
                $('#tbody').empty();
                $('#tbody').append(row_tmpl);
                Loket.attachEvent2ElementbyClass('antri');   
            }           
        }
    },
    handleError:function(data){
        console.log(data);
    }
}

var Monitor={
    element_no_antrian:null,
    element_loket:null,
    runMonitor:function(element_no_antrian_id, element_loket_id){
        // assign element html
        this.element_no_antrian = $('#'+element_no_antrian_id);
        this.element_loket = $('#'+element_loket_id);
        

        // run pooling to get no antrian from server
        setInterval(function(){
            ExecRemote(monitor_url,updateDisplay,handleError,data)
        },delay_pooling);
    },
    updateDisplay:function(data){       
        if(data.is_ok){
            this.element_no_antrian.text(data.data.no_antrian);
            this.element_loket.text(data.data.loket);                
        }else{
            console.log(data.msg);
        }
        
    },
    handleError:function(data){
        console.log(data);
    }
}

var NoTiketGenerator={
    element_no_antrian:null,
    element_layanan:null,
    element_nama_layanan:null,
    btn_cls_name :'', 
    attr_kodelayanan:'',
    default_delay_execution:500,
    init:function(btn_cls, attr_kodelayanan, element_no_antrian_id, element_layanan_id, element_nama_layanan_id){
        this.element_no_antrian = $('#'+element_no_antrian_id);
        this.element_layanan = $('#'+element_layanan_id);
        this.element_nama_layanan = $('#'+element_nama_layanan_id);
        this.btn_cls_name = btn_cls;
        this.attr_kodelayanan = attr_kodelayanan;
    },
    attachEvent2ElementbyClass:function(){
        console.log(this.btn_cls_name);
        var a = this.attr_kodelayanan;        
        var updateDisplay = this.updateDisplay;
        var handleError = this.handleError;
        $('.'+this.btn_cls_name).click(function(){
            console.log(a);
            var kode_layanan = $(this).attr(a);
            var data={kodelayanan:kode_layanan};            
            ExecRemote(tiket_generator_url,updateDisplay,handleError,data);
            NoTiketGenerator.disableAllBtn();
        });
    },
    disableAllBtn:function(){
        console.log('as');
        var target = $('.'+this.btn_cls_name);
        console.log(target);
        target.each(function(){
            $(this).attr('disabled',true);
        })
    },
    updateDisplay:function(data){       
        console.log(data);

        if(data.is_ok){
            NoTiketGenerator.element_no_antrian.text(data.data.no_antrian);
            NoTiketGenerator.element_layanan.text(data.data.kodelayanan);
            NoTiketGenerator.element_nama_layanan.text(data.data.nama_layanan);
            NoTiketGenerator.element_no_antrian.show();
            NoTiketGenerator.element_layanan.show();
            NoTiketGenerator.element_nama_layanan.show(); 

        }else{            
            
            NoTiketGenerator.element_no_antrian.show();
            NoTiketGenerator.element_layanan.show();
            NoTiketGenerator.element_nama_layanan.show(); 
        }
        NoTiketGenerator.enableBtnWithDelay()
        
    },
    clearInfoAntrian:function(){
        //NoTiketGenerator.element_layanan.text('');
        NoTiketGenerator.element_layanan.hide(3000);
        //NoTiketGenerator.element_no_antrian.hide('');
        NoTiketGenerator.element_no_antrian.hide(3000);
        NoTiketGenerator.element_nama_layanan.hide(3000);
    },

    handleError:function(data){
        console.log(data);
    },

    enableBtnWithDelay:function(){
        setTimeout(function(){
            $('.'+NoTiketGenerator.btn_cls_name).each(function(){
                $(this).removeAttr('disabled');                
            });
            NoTiketGenerator.clearInfoAntrian();
        }, NoTiketGenerator.default_delay_execution);
    }
}






