var monitor_url = '/antrian/monitor/'
var MonitorModel={
	number2sound:function(number){

		if(number <= 11){
			return [number];
		}else if(number < 20){
			sisa = number-10;
			return [sisa, 'belas'];
		}
		else if(number < 100){
			var satuan = number % 10;
				var puluhan = Math.floor(number / 10);
				if (satuan < 1){
					return [puluhan,'puluh'];
				}
				return [puluhan,'puluh',satuan]
		}
		else if(number < 1000){
			var sisa = number %100;
				var ratusan = Math.floor(number / 100);
				var sisa_sisa =[];
				if (sisa < 1){
					sisa_sisa=[];
				}
				else{
					sisa_sisa= MonitorModel.number2sound(sisa);
				}
				if(ratusan ==1){
					hasil = ['seratus'];
					return hasil.concat(sisa_sisa);
				}
				hasil = [ratusan,'ratus'];
				return hasil.concat(sisa_sisa);
				
		}		

	},
	playSound:function(number){
		var files = MonitorModel.number2sound(number);
		console.log(number);
		var sounds=new Array();
          var i=-1;

          for(var c=0; c< files.length;c++){
            sounds.push(new Audio('/media/rekaman/'+files[c]+'.3gp'));
          };
          playSnd();
          function playSnd() {
              i++;
              if (i == sounds.length) return;
              sounds[i].addEventListener('ended', playSnd);
              sounds[i].play();
          };
	},
	subscribeMonitor:function(kode_poli){
		var data={kode_poli:kode_poli, action:'get_no_tiket'}
        setInterval(function(){
            ExecRemote(monitor_url,MonitorModel.updateDisplay,MonitorModel.handleError, data)
        },1000);       

        
	},
	updateDisplay:function(resp){
		console.log(resp);
		if(resp.is_ok){
			if(resp.data){
				$('#no_antrian').text(resp.data.no_antrian);
				$('#nama_loket').text(resp.data.nama_loket);
				$('#next_antrian').text(resp.data.next_antrian);
				if(resp.data.was_calling==0){
					MonitorModel.playSound(resp.data.no_antrian);
					var data2={kode_poli:kode_poli, action:'set_was_playing'};
					ExecRemote(monitor_url,MonitorModel.onSetWasPlaying,MonitorModel.handleError, data2);
				}				

			}else{
				$('#no_antrian').text('-');
				$('#nama_loket').text('-');
				$('#next_antrian').text('-');
			}
			
		}else{
			console.log(resp.msg);
		}
	}, 
	onSetWasPlaying:function(resp){
		if(resp.is_ok){
			//do nathing
		}else{
			console.log(dat.msg);
		}
	},
	handleError:function(data){
		console.log(data);
	}

}