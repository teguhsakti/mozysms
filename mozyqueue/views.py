from django.http import HttpResponse, HttpResponseRedirect	
from django.shortcuts import render
from mozysms.models import Poli 
from  django.db.models import Q
from .models import Antrian, UserLoket, MonitorAntrian, StatusAntrian
from django.views.decorators.csrf import csrf_exempt
from .services.noantriangeneratorservices import generate_no_antrian
from django.contrib.auth.decorators import login_required, permission_required
import json
import datetime
import random

min_dt = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
max_dt = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

@csrf_exempt
def monitor(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.groups.filter(name='loket'):
		return HttpResponseRedirect('/')
	userloket = UserLoket.objects.filter(user_auth=request.user).first()
	if request.is_ajax():
		mimetype = 'application/json'
		data = json.loads(request.body)
		kode_poli =data['kode_poli']
		action=data['action']
		if action == 'get_no_tiket':			
			monitor = MonitorAntrian.objects.filter(Q(nama_loket=userloket.nama_loket) & Q(antrian__kode_poli=kode_poli) & Q(antrian__created_at__range=(min_dt, max_dt))).first()
			antrian = Antrian.objects.filter(Q(kode_poli=kode_poli) & Q(status=1) & Q(created_at__range = (min_dt, max_dt))).first()
			next_antrian = antrian.no_antrian
			result=None
			if monitor:
				result ={'is_ok':True, 'msg':'ok','code':0, 'data':{'no_antrian':monitor.antrian.no_antrian,'nama_loket':monitor.nama_loket,'was_calling':monitor.was_calling,'next_antrian':next_antrian}}	
			else:
				result ={'is_ok':True, 'msg':'ok','code':0, 'data':None}	
			return HttpResponse(json.dumps(result), mimetype)

		elif action =='set_was_playing':
			monitor = MonitorAntrian.objects.filter(Q(nama_loket=userloket.nama_loket) & Q(antrian__kode_poli=kode_poli) & Q(antrian__created_at__range=(min_dt, max_dt))).first()
			result = None
			if monitor:
				monitor.was_calling=1
				monitor.save()
				result ={'is_ok':True, 'msg':'ok','code':0, 'data':None}	
			else:
				result ={'is_ok':False, 'msg':'data not found','code':0, 'data':None}	
			return HttpResponse(json.dumps(result), mimetype)
	return render(request, 'antrian/monitor.html',{'userloket':userloket})

@csrf_exempt
def tiket(request):
	
	if request.is_ajax():
		mimetype = 'application/json'
		data = json.loads(request.body) 		
		kodelayanan = data['kodelayanan']
		result ={}
		try:
			no_antrian = generate_no_antrian(kodelayanan)
			antrian = Antrian.objects.get(Q(kode_poli=kodelayanan) & Q(no_antrian=no_antrian) & Q(created_at__range=(min_dt, max_dt)))
			nama_poli = antrian.nama_poli
			result = {'is_ok':True,'code':0, 'msg':'ok', 'data':{'kodelayanan':kodelayanan,'no_antrian':no_antrian, 'nama_layanan':nama_poli}}
		except Exception as e:
			result = {'is_ok':False,'code':1, 'msg':e.message, 'data':{}}			

		data = json.dumps(result)			
		return HttpResponse(data, mimetype)
	layanans = Poli.objects.all()
	return render(request, 'antrian/antrian.html',{'layanans':layanans})

@csrf_exempt

def loket(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.groups.filter(name='loket'):
		return HttpResponseRedirect('/')

	userloket = UserLoket.objects.filter(user_auth=request.user).first()
	if request.is_ajax():
		mimetype='application/json'
		data = json.loads(request.body)
		
		action = data['action']
		if action == 'list':
			kode_poli = data['kode_poli']
			antrians = Antrian.objects.filter(Q(kode_poli=kode_poli) & Q(status=StatusAntrian.ANTRI) & Q(created_at__range=(min_dt, max_dt)))[:10]
			results =[]
			for a in antrians:
				result={'no_antrian':a.no_antrian, 'status':a.status,'id':a.id}
				results.append(result)
			resp = {'is_ok':True, 'code':0, 'msg':'ok','data':results}
			return HttpResponse(json.dumps(resp), mimetype)
		elif action =='get_current_antrian':
			kode_poli = data['kode_poli']
			monitor = MonitorAntrian.objects.filter(Q(nama_loket=userloket.nama_loket) & Q(antrian__kode_poli=kode_poli) & Q(antrian__created_at__range=(min_dt, max_dt))).first()	
			if monitor:
				antrian = monitor.antrian
				result = {'no_antrian':antrian.no_antrian, 'status':antrian.status, 'id':antrian.id,'server_time':str(datetime.datetime.now())}
				resp = {'is_ok':True, 'code':0, 'msg':'ok','data':result}
				return HttpResponse(json.dumps(resp), mimetype)
			else:
				resp = {'is_ok':True, 'code':0, 'msg':'ok', 'data':None}
				return HttpResponse(json.dumps(resp), mimetype)

		#hilman
		elif action =='get_antrian_terakhir':
			kode_poli = data['kode_poli']
			antrians = Antrian.objects.filter(Q(kode_poli=kode_poli) & Q(status=StatusAntrian.TUNDA) & Q(created_at__range=(min_dt, max_dt)) )[:5]
			results = []
			for a in antrians:
				print 'status : ' + str(a.status)
				result={'no_antrian':a.no_antrian, 'status':a.status, 'id':a.id}
				results.append(result)
			resp={'is_ok':True, 'code':0, 'msg':'ok', 'data':results}
			return HttpResponse(json.dumps(resp), mimetype)
		#endhilman
			
		elif action=='panggil':
			antrian_id = data['antrian_id']
			id =int(antrian_id)
			antrian = Antrian.objects.get(id=id)
			monitor = MonitorAntrian.objects.filter(Q(antrian=antrian) & Q(nama_loket=userloket.nama_loket)).first()
			if monitor:
				monitor.was_calling = False
			else:
				monitor = MonitorAntrian(antrian=antrian, nama_loket = userloket.nama_loket, was_calling = False)
			monitor.save()
			antrian.status = StatusAntrian.PANGGIL
			antrian.save()
			resp = {'is_ok':True, 'code':0, 'msg':'ok','data': None}
			return HttpResponse(json.dumps(resp), mimetype)
		elif action == 'done':
			antrian_id = data['antrian_id']
			id =int(antrian_id)
			antrian = Antrian.objects.get(id=id)
			monitor = MonitorAntrian.objects.filter(Q(antrian=antrian) & Q(nama_loket=userloket.nama_loket)).first()
			if monitor:
				monitor.delete()
			if antrian:
				antrian.status = StatusAntrian.DONE
				antrian.save()
			resp = {'is_ok':True, 'code':0, 'msg':'ok','data': None}
			return HttpResponse(json.dumps(resp), mimetype)
		elif action == 'tunda':
			antrian_id = data['antrian_id']
			id = int(antrian_id)
			antrian = Antrian.objects.get(id=id)
			monitor = MonitorAntrian.objects.filter(Q(antrian=antrian) & Q(nama_loket=userloket.nama_loket))
			print antrian
			print monitor
			if monitor:
				monitor.delete()
			if antrian:
				antrian.status = StatusAntrian.TUNDA
				antrian.save()
			resp={'is_ok':True, 'code':0, 'msg':'ok', 'data': None}
			return HttpResponse(json.dumps(resp), mimetype)
		elif action == 'reset':
			antrian_id = data['antrian_id']
			id = int(antrian_id)
			antrian = Antrian.objects.get(id=id)
			print 'udah direset'
			if antrian:
				antrian.status = StatusAntrian.ANTRI
				antrian.save()
			resp={'is_ok':True, 'code':0, 'msg':'ok', 'data': None}
			return HttpResponse(json.dumps(resp), mimetype)
		else:
			return None
	antrians = Antrian.objects.all()
	return render(request, 'antrian/loket.html',{'antrians':antrians,'userloket':userloket,'user':request.user})