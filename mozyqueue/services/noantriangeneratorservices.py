from mozyqueue.models import Antrian, StatusAntrian
from mozysms.models import Poli
from django.db.models import Q
import datetime


min_dt = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
max_dt = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

def generate_no_antrian(kode_layanan):
	# validate kode layanan
	poli = Poli.objects.filter(kode_poli = kode_layanan).first()
	if poli:
		#to do tambahkan filter time
		sms_reserved_ticket = poli.jumlah_nomor
		count = Antrian.objects.filter(Q(kode_poli=kode_layanan) & Q(created_at__range=(min_dt, max_dt)) & Q(no_antrian__gte = sms_reserved_ticket)).count()
		#count = Antrian.objects.filter(kode_poli=kode_layanan).count()
		print 'ini jumlah count' + str(count)
		count = sms_reserved_ticket+count+1
		if count > (poli.no_antrian_max - 1):
			raise Exception('no antrian melebihi batas maximum')
		antrian = Antrian(kode_poli=poli.kode_poli,nama_poli=poli.nama,no_antrian=count, status = StatusAntrian.ANTRI)
		antrian.save()
		return count
	raise Exception('poli tidak terdaftar')


