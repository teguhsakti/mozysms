# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('mozysms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Antrian',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kode_poli', models.CharField(unique=True, max_length=50)),
                ('nama_poli', models.CharField(unique=True, max_length=150)),
                ('no_antrian', models.IntegerField()),
                ('status', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='MonitorAntrian',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nama_loket', models.CharField(max_length=150)),
                ('was_calling', models.BooleanField(default=False)),
                ('antrian', models.ForeignKey(to='mozyqueue.Antrian')),
            ],
        ),
        migrations.CreateModel(
            name='UserLoket',
            fields=[
                ('user_auth', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('nama_loket', models.CharField(max_length=100)),
                ('layanan', models.ForeignKey(to='mozysms.Poli')),
            ],
        ),
        migrations.CreateModel(
            name='UserMonitor',
            fields=[
                ('user_auth', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('nama_monitor', models.CharField(max_length=100)),
                ('layanan', models.ForeignKey(to='mozysms.Poli')),
            ],
        ),
    ]
