from django.db import models
from mozysms.models import Poli
from django.contrib.auth.models import User

# Create your models here.

class UserLoket(models.Model):	
	user_auth = models.OneToOneField(User, primary_key=True)
	layanan = models.ForeignKey(Poli)	
	nama_loket = models.CharField(max_length=100)
	def __str__(self):
		return self.layanan.nama+' - '+ self.nama_loket

class UserMonitor(models.Model):
	user_auth = models.OneToOneField(User, primary_key=True)
	layanan = models.ForeignKey(Poli)
	nama_monitor = models.CharField(max_length=100)
	def __str__(self):
		return self.layanan.nama+' - '+ self.nama_monitor

class Antrian(models.Model):
	kode_poli = models.CharField(max_length=50, unique=True)
	nama_poli = models.CharField(max_length=150, unique=True)
	no_antrian = models.IntegerField()
	status = models.IntegerField()
	created_at= models.DateTimeField(auto_now=True)

class MonitorAntrian(models.Model):
	# kode_poli = models.CharField(max_length=50)
	# nama_poli = models.CharField(max_length=150)
	nama_loket = models.CharField(max_length=150)
	# nomor_antrian = models.IntegerField()
	antrian = models.ForeignKey(Antrian)
	was_calling = models.BooleanField(default=False)

class StatusAntrian():
	ANTRI=1
	PANGGIL=2
	TUNDA = 3
	DONE = 4