from rapidsms.apps.base import AppBase
from pprint import pprint	

class PingPong(AppBase):
	def handle(self, msg):

		print "============="
		print msg.connection.identity
		print "=============="
		if msg.text.lower() == 'ping':
			msg.respond('pong')
			return True
		elif msg.text.lower() == 'asri':
			msg.respond('ganteng')
			return True
		elif msg.text.lower() == 'sigit':
			msg.respond('hello pak sigit')
			return True
		return False

	